import '../scss/toast.scss';
import Classes from './constants/Classes';
import Toast from './Toast';
import ToastCollection from './ToastCollection';
import EventNames from './constants/EventNames';
import Durations from './constants/Durations';
import type { SwipeEvent } from './types/SwipeEvent';
import Directions from './constants/Directions';
import PluginNames from './constants/PluginNames';

export default class ToastManager {
  private readonly _closeHandler: (e: Event) => void;
  private readonly _config: Config;
  public activeToasts: ToastCollection;
  public template: ((data: any) => string) | undefined;

  /**
   * Create a new manager to manage your toasts
   */
  constructor(config?: OptionalConfig) {
    this._config = {
      container: config?.container || document.body,
      seconds: config?.seconds || 3,
      spacing: config?.spacing || 20,
      offset: config?.offset || 0,
      swipe: config?.swipe !== false,
    };

    this.activeToasts = new ToastCollection();
    this._closeHandler = this.closeHandler.bind(this);
  }

  /**
   * Show a specific template according to the status
   */
  public notify(data: any): void {
    const plugins: string[] = this.config.swipe ? [PluginNames.SWIPE] : [];
    const toast: Toast = new Toast(data, plugins, this.template);

    this.add(toast);

    window.getComputedStyle(toast.element).top; // Needed for transitions to always work, recalculates top property
    this.repositionAll();

    setTimeout(() => {
      toast.hide();
    }, this._config.seconds * 1000 - Durations.TRANSITION_IN_MILLISECONDS);

    setTimeout(() => {
      this.destroy(toast);
    }, this._config.seconds * 1000);
  }

  /**
   * Add a toast
   */
  public add(toast: Toast): void {
    this._config.container.appendChild(toast.element);
    this.activeToasts.add(toast);
    this.addEventListeners(toast);
  }

  /**
   * Destroy the given toast
   */
  public destroy(toast: Toast): void {
    toast.destroy();
    this.activeToasts.delete(toast);
    this.removeEventListeners(toast);
  }

  /**
   * Calculate and give all toasts a new position
   */
  public repositionAll(): void {
    const orderShowed: Toast[] = Array.from(this.activeToasts.all()).reverse();

    orderShowed.forEach((toast: Toast, index: number) => {
      const totalOfSpacings: number = (index + 1) * this.config.spacing;
      let totalOfElementsHeight: number = 0;

      for (let i = 0; i < index; i++) {
        totalOfElementsHeight += toast.element.offsetHeight;
      }

      toast.element.style.top = this.config.offset + totalOfSpacings + totalOfElementsHeight + 'px';
    });
  }

  /**
   * Handler to close a toast
   */
  private closeHandler(e: Event): void {
    const target = e.target as HTMLElement | null;
    if (!target) return;

    const element: HTMLElement | null = target.closest(`.${Classes.TOAST}`);
    if (!element) return;

    const toast = this.activeToasts.findByElement(element);
    if (!toast) return;

    this.destroy(toast);
    this.repositionAll();
  }

  /**
   * Add all event listeners to the given toast
   */
  private addEventListeners(toast: Toast): void {
    if (this.config.swipe) {
      toast.enablePlugins();
      toast.element.addEventListener(EventNames.SWIPE_LEFT, this.onSwipeLeft.bind(this));
      toast.element.addEventListener(EventNames.SWIPE_RIGHT, this.onSwipeRight.bind(this));
    }

    const closeElement: HTMLElement | null = toast.element.querySelector(`.${Classes.CLOSE_BUTTON}`);
    if (!closeElement) return;

    closeElement.addEventListener(EventNames.CLICK, this._closeHandler);
  }

  /**
   * Remove all event listeners from the given toast
   */
  private removeEventListeners(toast: Toast): void {
    toast.disablePlugins();
    toast.element.removeEventListener(EventNames.SWIPE_LEFT, this.onSwipeLeft);
    toast.element.removeEventListener(EventNames.SWIPE_RIGHT, this.onSwipeRight);

    const closeElement: HTMLElement | null = toast.element.querySelector(`.${Classes.CLOSE_BUTTON}`);
    if (!closeElement) return;

    closeElement.removeEventListener(EventNames.CLICK, this._closeHandler);
  }

  /**
   * Executes when the user has swiped left on a toast
   */
  private onSwipeLeft(event: Event): void {
    const swipeEvent = event as SwipeEvent;
    this.onSwipe(swipeEvent.detail.toast, Directions.LEFT);
  }

  /**
   * Executes when the user has swiped right on a toast
   */
  private onSwipeRight(event: Event): void {
    const swipeEvent = event as SwipeEvent;
    this.onSwipe(swipeEvent.detail.toast, Directions.RIGHT);
  }

  /**
   * Handle swipe gestures
   */
  private onSwipe(toast: Toast, direction: string) {
    if (direction === Directions.RIGHT) {
      toast.element.style.right = '-100vw';
      toast.element.style.left = 'initial';
    }

    if (direction === Directions.LEFT) {
      toast.element.style.left = '-100vw';
      toast.element.style.right = 'initial';
    }

    setTimeout(() => {
      this.destroy(toast);
      this.repositionAll();
    }, Durations.TRANSITION_IN_MILLISECONDS);
  }

  /**
   * Getter for the config property
   */
  public get config(): Config {
    return this._config;
  }
}
