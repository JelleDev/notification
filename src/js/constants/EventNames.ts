export default {
  CLICK: 'click',
  TOUCH_START: 'touchstart',
  TOUCH_MOVE: 'touchmove',
  SWIPE_LEFT: 'swipeLeft',
  SWIPE_RIGHT: 'swipeRight',
};
