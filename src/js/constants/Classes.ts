export default {
  TOAST: 'toast',
  HEADER: 'toast__header',
  BODY: 'toast__body',
  CLOSE_BUTTON: 'toast__close',
};
