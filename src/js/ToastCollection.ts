import Toast from './Toast';

export default class ToastCollection {
  private readonly _toasts: Toast[];

  /**
   * Create a new collection of toasts
   */
  constructor(toasts: Toast[] = []) {
    this._toasts = toasts;
  }

  /**
   * Return the first toast
   */
  first(): Toast {
    return this._toasts[0];
  }

  /**
   * Find a toast by a given index
   */
  find(index: number): Toast | undefined {
    return this._toasts[index];
  }

  /**
   * Find a toast in the collection by a HTMLElement
   */
  findByElement(element: HTMLElement): Toast | undefined {
    return this._toasts.find((toast: Toast) => {
      return toast.element === element;
    });
  }

  /**
   * Get all the _toasts
   */
  all(): Toast[] {
    return this._toasts;
  }

  /**
   * Add a toast to the collection
   */
  add(toast: Toast): void {
    this._toasts.push(toast);
  }

  /**
   * Remove the toast from the collection
   */
  delete(toast: Toast): void {
    this._toasts.splice(this._toasts.indexOf(toast), 1);
  }
}
