interface DynamicAccessible {
  [key: string]: any
}
