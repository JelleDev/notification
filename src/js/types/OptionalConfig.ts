interface OptionalConfig {
  container?: HTMLElement,
  seconds?: number,
  spacing?: number,
  offset?: number,
  swipe?: boolean,
}
