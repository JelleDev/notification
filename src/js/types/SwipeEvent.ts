import Toast from '../Toast';

export interface SwipeEvent extends CustomEvent {
  detail: {
    toast: Toast
  }
}
