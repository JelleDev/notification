import '../scss/toast.scss';
import Classes from './constants/Classes';
import SwipePlugin from './plugins/SwipePlugin';

export default class Toast {
  public element: HTMLElement;
  public plugins: ToastPlugin[];

  /**
   * Create a new toast
   */
  constructor(
    data: any,
    plugins: string[] = [],
    template: ((data: any) => string) | undefined = undefined
  ) {
    this.plugins = this.collectPlugins(plugins);

    const wrapper: HTMLDivElement = document.createElement('div');
    wrapper.innerHTML = template ? template(data) : this.template(data);
    const content = wrapper.firstElementChild as HTMLElement | null;

    if (!content) {
      console.warn('No toast element found.. Created a plain toast for you');
      this.element = wrapper;
      return;
    }

    this.element = content;
  }

  /**
   * Instantiate the template
   */
  public template({className, title, content}: any): string {
    return `
      <div class="${Classes.TOAST} ${className}">
        <div class="${Classes.HEADER}">
          <span>${title}</span>
          <button class="${Classes.CLOSE_BUTTON}">
            <i class="far fa-window-close"></i>
          </button>
        </div>
        <div class="${Classes.BODY}">
          <p>${content}</p>
        </div>
      </div>
    `;
  }

  /**
   * Hide the toast
   */
  public hide(): void {
    this.element.style.opacity = '0';
  }

  /**
   * Destroy the toast
   */
  public destroy(): void {
    this.element.remove();
  }

  /**
   * Activate the plugins
   */
  public enablePlugins(): void {
    this.plugins.forEach((plugin: ToastPlugin) => plugin.enable());
  }

  /**
   * Disable the plugins
   */
  public disablePlugins(): void {
    this.plugins.forEach((plugin: ToastPlugin) => plugin.disable());
  }

  /**
   * Collect and instantiate the plugins available for the toast
   */
  private collectPlugins(plugins: string[]): ToastPlugin[] {
    const possiblePlugins: DynamicAccessible = {
      swipe: SwipePlugin,
    };

    return plugins
      .map((plugin: string) => {
        if (!possiblePlugins[plugin]) return;
        return new possiblePlugins[plugin](this);
      })
      .filter(Boolean);
  }
}
