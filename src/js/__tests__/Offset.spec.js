import ToastManager from '../ToastManager';
import { click, swipeLeft } from '../helpers/tests';
import Classes from '../constants/Classes';
import Durations from '../constants/Durations';

Object.defineProperty(HTMLElement.prototype, 'offsetHeight', {
  get: function() {
    return parseInt(this.style.height || 60);
  },
});

const toastHeight = 60;
let toastManager;

beforeEach(() => {
  toastManager = new ToastManager();
  jest.useFakeTimers();
});

afterEach(() => document.body.innerHTML = '');

describe('When there is one active toast', () => {
  it('Should have a offset of the spacing configuration', () => {
    toastManager.notify(0);
    jest.advanceTimersByTime(1); // TODO: Make sure we dont have to use this hack because of the used timeouts

    expect(toastManager.activeToasts.first().element.style.top).toBe(`${toastManager.config.spacing}px`);
  });
});

describe('When there are two toasts', () => {
  beforeEach(() => {
    toastManager.notify(0);
    jest.advanceTimersByTime(1);
    toastManager.notify(1);
    jest.advanceTimersByTime(1);
  });

  it('Sets the offset of the 1st toast to a calculated offset', () => {
    const expectedOffset = 2 * toastManager.config.spacing + toastHeight;
    expect(toastManager.activeToasts.first().element.style.top).toBe(`${expectedOffset}px`);
  });

  it('Sets the offset of the 2nd toast to the spacing configuration', () => {
    expect(toastManager.activeToasts.find(1).element.style.top).toBe(`${toastManager.config.spacing}px`);
  });

  it('Should reposition when the users manually closes a toast',() => {
    click(toastManager.activeToasts.find(1).element.querySelector(`.${Classes.CLOSE_BUTTON}`));
    expect(toastManager.activeToasts.first().element.style.top).toBe(`${toastManager.config.spacing}px`);
  });

  it('Should reposition when the user swipes a toast', () => {
    swipeLeft(toastManager.activeToasts.find(1).element);
    jest.advanceTimersByTime(Durations.TRANSITION_IN_MILLISECONDS);
    expect(toastManager.activeToasts.first().element.style.top).toBe(`${toastManager.config.spacing}px`);
  });
});

