import Toast from '../Toast';
import SwipePlugin from '../plugins/SwipePlugin';
import PluginNames from '../constants/PluginNames';

let toast;

beforeEach(() => {
  toast = new Toast({ className: 'active', title: 'test', content: 'test' }, [PluginNames.SWIPE]);
  jest.useFakeTimers();
});

it('Has a template', () => {
  expect(toast.template).not.toBeUndefined();
});

it('Should allow to override the template of the toast', () => {
  const testTemplate = () => '<div>test</div>';
  const toast = new Toast(null, [], testTemplate);
  expect(toast.element.outerHTML).toBe(testTemplate());
});

it('Should still create a plain toast if the given template was not valid', () => {
  const testTemplate = () => 'test';
  expect(new Toast(null, [], testTemplate).element.outerHTML).toBe('<div>test</div>');
});

it('Should only allow for valid plugins', () => {
  const toast = new Toast(
    { className: 'active', title: 'test', content: 'test' },
    ['swipe', 'invalid', undefined]
  );

  expect(toast.plugins).toHaveLength(1);
  expect(toast.plugins[0]).toBeInstanceOf(SwipePlugin);
});

it('Should be able to enable plugins', () => {
  toast.plugins[0].enable = jest.fn();
  toast.enablePlugins();
  expect(toast.plugins[0].enable).toHaveBeenCalled();
});

it('Should be able to disable plugins', () => {
  toast.plugins[0].disable = jest.fn();
  toast.disablePlugins();
  expect(toast.plugins[0].disable).toHaveBeenCalled();
});
