import ToastCollection from '../ToastCollection';
import Toast from '../Toast';

let toasts;

beforeEach(() => {
  toasts = new ToastCollection([ new Toast({}) ]);
  jest.useFakeTimers();
});

it('Should be able to get the first item from the collection', () => {
  expect(toasts.first()).toBeInstanceOf(Toast);
});

it('Should be able to find a toast according to a given html element', () => {
  expect(toasts.find(0)).toBeInstanceOf(Toast);
  expect(toasts.find(1)).toBeUndefined();
});

it('Should be able to find a toast according to a given html element', () => {
  expect(toasts.findByElement(toasts.first().element)).toBeInstanceOf(Toast);
});

it('Should be able to get all the toasts from the collection', () => {
  expect(toasts.all()).toHaveLength(1);
});

it('Should be able to add a toast to the collection', () => {
  toasts.add(new Toast({}));
  expect(toasts.all()).toHaveLength(2);
});

it('Should be able to delete a toast from the collection', () => {
  toasts.delete(toasts.first());
  expect(toasts.all()).toHaveLength(0);
});
