import ToastManager from '../ToastManager';
import { click, swipeLeft, swipeRight } from '../helpers/tests';
import Classes from '../constants/Classes';
import Durations from '../constants/Durations';

let toastManager;

beforeEach(() => {
  toastManager = new ToastManager();
  jest.useFakeTimers();
});

it('Should only show a template for an X amount of seconds', () => {
  toastManager.notify({ className: 'active', title: 'test', content: 'test' });

  let template = document.querySelector('div');
  expect(template).not.toBeNull();

  jest.runAllTimers();

  template = document.querySelector('div');
  expect(template).toBeNull();
});

it('Should keep track of all the active toasts', () => {
  toastManager.notify({ className: 'active', title: 'test', content: 'test' });
  expect(toastManager.activeToasts.all().length).toBe(1);

  jest.runAllTimers();

  expect(toastManager.activeToasts.all().length).toBe(0);
});

it('Should be able to close a given toast by clicking an element', () => {
  toastManager.notify({ className: 'active', title: 'test', content: 'test' });
  jest.advanceTimersByTime(1);

  const closeElement = toastManager.activeToasts.first().element.querySelector(`.${Classes.CLOSE_BUTTON}`);
  click(closeElement);
  expect(toastManager.activeToasts.first()).toBeUndefined();
});

it('Should allow to override the template of the toast', () => {
  toastManager.template = (content) => `<div>${content}</div>`;
  toastManager.notify('changed!');

  expect(toastManager.activeToasts.first().element.innerHTML).toBe('changed!');
});

describe('When swiped', () => {
  describe('When swiping left', () => {
    beforeEach(() => {
      toastManager.notify({ className: 'active', title: 'test', content: 'test' });
      swipeLeft(toastManager.activeToasts.first().element);
    });

    it('Should move and delete', () => {
      expect(toastManager.activeToasts.first().element.style.left).toBe('-100vw');
      jest.advanceTimersByTime(Durations.TRANSITION_IN_MILLISECONDS);
      expect(toastManager.activeToasts.first()).toBeUndefined();
    });
  });

  describe('When swiping right', () => {
    beforeEach(() => {
      toastManager.notify({className: 'active', title: 'test', content: 'test'});
      swipeRight(toastManager.activeToasts.first().element);
    });

    it('Should move and delete', () => {
      expect(toastManager.activeToasts.first().element.style.right).toBe('-100vw');
      jest.advanceTimersByTime(Durations.TRANSITION_IN_MILLISECONDS);
      expect(toastManager.activeToasts.first()).toBeUndefined();
    });
  });
});

it('Should remove all event listeners for an element', () => {
  toastManager.destroy = jest.fn();
  toastManager.notify({ className: 'active', title: 'test', content: 'test' });

  const closeElement = toastManager.activeToasts.first().element.querySelector(`.${Classes.CLOSE_BUTTON}`);

  toastManager.removeEventListeners(toastManager.activeToasts.first());

  click(closeElement);
  expect(toastManager.destroy).not.toHaveBeenCalled();

  swipeLeft(toastManager.activeToasts.first().element);
  jest.advanceTimersByTime(Durations.TRANSITION_IN_MILLISECONDS);
  expect(toastManager.destroy).not.toHaveBeenCalled();
});
