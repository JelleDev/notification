import ToastManager from '../ToastManager';

let toastManager;

it('Should allow the seconds configuration to be configurable', () => {
  toastManager = new ToastManager({ seconds: 5 });
  expect(toastManager.config.seconds).toBe(5);
});

it('Should allow the spacings configuration to be configurable', () => {
  toastManager = new ToastManager({ spacing: 40 });
  expect(toastManager.config.spacing).toBe(40);
});

it('Should allow the offset configuration to be configurable', () => {
  toastManager = new ToastManager({ offset: 60 });
  expect(toastManager.config.offset).toBe(60);
});

describe('Swipe configuration', () => {
  it('Should add swipe functionality as a default', () => {
    toastManager = new ToastManager();
    toastManager.notify({ className: 'active', title: 'test', content: 'test' });

    expect(toastManager.config.swipe).toBeTruthy();
    expect(toastManager.activeToasts.first().plugins).toHaveLength(1);
  });

  it('Should not add swipe functionality if not configured that way', () => {
    toastManager = new ToastManager({ swipe: false });
    toastManager.notify({ className: 'active', title: 'test', content: 'test' });

    expect(toastManager.config.swipe).toBeFalsy();
    expect(toastManager.activeToasts.first().plugins).toHaveLength(0);
  });
});
