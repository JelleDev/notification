import '../scss/demo.scss';
import ToastManager from './ToastManager';

const toastManager = new ToastManager({ seconds: 3600, offset: 200 });

window.toastManager = toastManager;
