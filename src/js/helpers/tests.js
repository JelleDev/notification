import EventNames from '../constants/EventNames';
import Directions from '../constants/Directions';

/**
 * Simulate a click
 */
export function click(element) {
  const event = new Event(EventNames.CLICK);
  event.eventName = EventNames.CLICK;

  element.dispatchEvent(event);
}

/**
 * Simulate a swipe
 */
export function swipe(element, direction) {
  const touchStartEvent = new Event(EventNames.TOUCH_START);
  touchStartEvent.eventName = EventNames.TOUCH_START;
  touchStartEvent.touches = [
    {
      clientX: 0,
      clientY: 0
    }
  ];

  const touchMoveEvent = new Event(EventNames.TOUCH_MOVE);
  touchMoveEvent.eventName = EventNames.TOUCH_MOVE;
  touchMoveEvent.touches = [
    {
      clientX: direction === Directions.RIGHT ? 50 : -50,
      clientY: 0
    }
  ];

  element.dispatchEvent(touchStartEvent);
  element.dispatchEvent(touchMoveEvent);
}

/**
 * Simulate a swipe to the left
 */
export function swipeLeft(element) {
  swipe(element, Directions.LEFT);
}

/**
 * Simulate a swipe to the right
 */
export function swipeRight(element) {
  swipe(element, Directions.RIGHT);
}
