import { swipeLeft, swipeRight } from '../../helpers/tests';
import Toast from '../../Toast';
import SwipePlugin from '../SwipePlugin';
import EventNames from '../../constants/EventNames';

it('Should send a swipe left notification when the user swiped left', () => {
  const ability = new SwipePlugin(new Toast({}));
  ability.enable();

  const listener = jest.fn();
  ability.toast.element.addEventListener(EventNames.SWIPE_LEFT, listener);

  swipeLeft(ability.toast.element);

  expect(listener).toHaveBeenCalledTimes(1);
});

it('Should send a swipe right notification when the user swiped right', () => {
  const ability = new SwipePlugin(new Toast({}));
  ability.enable();

  const listener = jest.fn();
  ability.toast.element.addEventListener(EventNames.SWIPE_RIGHT, listener);

  swipeRight(ability.toast.element);

  expect(listener).toHaveBeenCalledTimes(1);
});

it('Should not send more than 1 swipe notification on the same element', () => {
  const ability = new SwipePlugin(new Toast({}));
  ability.enable();

  const listener = jest.fn();
  ability.toast.element.addEventListener(EventNames.SWIPE_RIGHT, listener);

  swipeRight(ability.toast.element);
  swipeLeft(ability.toast.element);
  swipeRight(ability.toast.element);

  expect(listener).toHaveBeenCalledTimes(1);
});

it('Should destroy the swipe event listeners', () => {
  const ability = new SwipePlugin(new Toast({}));
  ability.enable();

  const listener = jest.fn();
  ability.toast.element.addEventListener(EventNames.SWIPE_RIGHT, listener);
  ability.toast.element.addEventListener(EventNames.SWIPE_LEFT, listener);

  ability.disable();

  swipeRight(ability.toast.element);
  swipeLeft(ability.toast.element);

  expect(listener).not.toHaveBeenCalled();
});
