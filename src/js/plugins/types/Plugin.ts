interface ToastPlugin {
  /**
   * Enable the plugin
   */
  enable(): void;

  /**
   * Disable the plugin
   */
  disable(): void;
}
