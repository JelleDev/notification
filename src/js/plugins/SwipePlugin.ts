import EventNames from '../constants/EventNames';
import Toast from '../Toast';

export default class SwipePlugin implements ToastPlugin {
  private _startTouchEvent?: TouchEvent;
  private readonly _startHandler: (event: TouchEvent) => void;
  private readonly _moveHandler: (event: TouchEvent) => void;
  public toast: Toast;
  public swiped: boolean = false;

  /**
   * Create a new swipe ability
   */
  constructor(toast: Toast) {
    this.toast = toast;
    this._startHandler = this.onTouchStart.bind(this);
    this._moveHandler = this.onTouchMove.bind(this);
  }

  /**
   * @inheritDoc
   */
  public enable(): void {
    this.toast.element.addEventListener('touchstart', this._startHandler);
    this.toast.element.addEventListener('touchmove', this._moveHandler);
  }

  /**
   * @inheritDoc
   */
  public disable(): void {
    this.toast.element.removeEventListener('touchstart', this._startHandler);
    this.toast.element.removeEventListener('touchmove', this._moveHandler);
  }

  /**
   * Executes when the touch start event is fired
   */
  private onTouchStart(event: TouchEvent): void {
    this._startTouchEvent = event;
  }

  /**
   * Executes when the touch move event is fired
   */
  private onTouchMove(event: TouchEvent): void {
    if (!this._startTouchEvent || this.swiped) return;

    const differenceHorizontal: number = event.touches[0].clientX - this._startTouchEvent.touches[0].clientX;
    const differenceVertical: number = event.touches[0].clientY - this._startTouchEvent.touches[0].clientY;

    if (!(Math.abs(differenceHorizontal) > Math.abs(differenceVertical))) return;

    this.swiped = true;

    if (differenceHorizontal > 0) {
      this.toast.element.dispatchEvent(
        new CustomEvent(EventNames.SWIPE_RIGHT, { detail: { toast: this.toast } })
      );
    } else {
      this.toast.element.dispatchEvent(
        new CustomEvent(EventNames.SWIPE_LEFT, { detail: { toast: this.toast } })
      );
    }
  }
}
